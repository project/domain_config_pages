<?php

namespace Drupal\domain_config_pages\Plugin\ConfigPagesContext;

use Drupal\config_pages\ConfigPagesContextBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\domain\DomainNegotiatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Domain config pages context.
 *
 * @ConfigPagesContext(
 *   id = "domain",
 *   label = @Translation("Domain"),
 * )
 */
class Domain extends ConfigPagesContextBase {

  /**
   * Domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DomainNegotiatorInterface $domain_negotiator, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->domainNegotiator = $domain_negotiator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('domain.negotiator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get the value of the context.
   *
   * @return string
   *   Return value of the context.
   */
  public function getValue(): string {
    return $this->domainNegotiator->getActiveId();
  }

  /**
   * Get the label of the context.
   *
   * @return string
   *   Return the label of the context.
   */
  public function getLabel(): string {
    return $this->domainNegotiator->getActiveDomain()->label();
  }

  /**
   * Get array of available links to switch on given context.
   *
   * @return array
   *   Return array of available links to switch on given context.
   */
  public function getLinks() {
    $links = [];
    $value = $this->getValue();
    /** @var \Drupal\domain\Entity\Domain[] $domains */
    $domains = $this->entityTypeManager->getStorage('domain')->loadMultiple();
    foreach ($domains as $domain) {
      $links[] = [
        'title' => $domain->label(),
        'href' => Url::fromUri($domain->getUrl()),
        'selected' => ($value == $domain->id()) ? TRUE : FALSE,
        'value' => $domain->id(),
      ];
    }

    return $links;
  }

}
