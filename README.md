# Domain Config Pages

The Domain Config Pages module provides a domain context plugin for config 
pages.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/domain_config_pages).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/domain_config_pages).

## Requirements

This module requires the following modules:
- [Domain](https://www.drupal.org/project/domain)
- [Config pages](https://www.drupal.org/project/config_pages)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the domain context for a config page on 
`admin/structure/config_pages/types/manage/<config_page>`.
2. Optionally, provide a default domain context.
